Source: libanyevent-fork-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Lucas Kanashiro <kanashiro@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: libanyevent-perl <!nocheck>,
                     libcommon-sense-perl <!nocheck>,
                     libio-fdpass-perl <!nocheck>,
                     libproc-fastspawn-perl <!nocheck>,
                     perl
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libanyevent-fork-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libanyevent-fork-perl.git
Homepage: https://metacpan.org/release/AnyEvent-Fork
Rules-Requires-Root: no

Package: libanyevent-fork-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libanyevent-perl,
         libcommon-sense-perl,
         libio-fdpass-perl,
         libproc-fastspawn-perl
Description: module to create new processes
 AnyEvent::Fork allows you to create new processes, without actually forking
 them from your current process (avoiding the problems of forking), but
 preserving most of the advantages of fork.
 .
 It can be used to create new worker processes or new independent subprocesses
 for short- and long-running jobs, process pools (e.g. for use in pre-forked
 servers) but also to spawn new external processes (such as CGI scripts from a
 web server), which can be faster (and more well behaved) than using fork+exec
 in big processes.
 .
 Special care has been taken to make this module useful from other modules,
 while still supporting specialised environments such as App::Staticperl or
 PAR::Packer.
